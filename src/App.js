import './App.css';
import {Container} from "semantic-ui-react";
import MainHeader from "./components/MainHeader";
import NewEntryForm from "./components/NewEntryForm";
import DisplayBalance from "./components/DisplayBalance";
import DisplayBalances from "./components/DisplayBalances";
import React, {useEffect, useState} from "react";
import EntryLines from "./components/EntryLines";
import ModalEdit from "./components/ModalEdit";
import {useSelector} from "react-redux";

function App() {

  // lifting the state up
  const [description, setDescription] = useState("");
  const [value, setValue] = useState("");
  const [isExpense, setIsExpense] = useState(true);
  const [isOpen, setIsOpen] = useState(false);
  const [entryId, setEntryId] = useState();
  const [totalIncome, setTotalIncome] = useState(0);
  const [totalExpenses, setTotalExpenses] = useState(0);
  const [totalBalance, setTotalBalance] = useState(0);

  // Hooks
  const entries = useSelector((state) => state.entries);

  useEffect(() => {
    if (!isOpen && entryId) {
      const index = entries.findIndex((entry) => entry.id === entryId);
      const newEntries = [...entries];
      newEntries[index].description = description;
      newEntries[index].value = value;
      newEntries[index].isExpense = isExpense;
      // setEntries(newEntries);
      resetEntry();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);

  // calculate total income and total expenses
  useEffect(() => {
    let totalIncome = 0;
    let totalExpenses = 0;
    entries.map(entry => {
        if(entry.isExpense) {
          return (totalExpenses += Number(entry.value));
        } else {
          return (totalIncome += Number(entry.value));
        }
      }
    );
    const roundedTotalBalance = (totalIncome - totalExpenses).toFixed(2);
    setTotalBalance(roundedTotalBalance);
    setTotalIncome(totalIncome.toFixed(2));
    setTotalExpenses(totalExpenses.toFixed(2));
  }, [entries]);

  // const addEntry = (description, value) => {...}
  function addEntry() {
    const result = entries.concat({
      id: entries.length + 1,
      description: description,
      value: value,
      isExpense: isExpense
    });
    // setEntries(result);
    resetEntry();
  }

  // const editEntry = (id) => {...}
  function editEntry(id) {
    console.log(`edit entry with id ${id}`);
    if (id) {
      const index = entries.findIndex((entry) => entry.id === id);
      const entry = entries[index];
      setEntryId(id);
      setDescription(entry.description);
      setValue(entry.value);
      setIsExpense(entry.isExpense);
      setIsOpen(true);
    }
  }

  function resetEntry() {
    setDescription("");
    setValue("");
    setIsExpense(true);
  }

  return (
    <Container>
      <MainHeader title={"Budget Tracker"} headerType={"h1"}/>
      <DisplayBalance name={"Your Balance: "} amount={totalBalance} color={"black"} size={"small"}/>
      <DisplayBalances totalIncome={totalIncome} totalExpenses={totalExpenses} />

      <MainHeader title={"History"} headerType={"h3"}/>
      <EntryLines
        entries={entries}
        editEntry={editEntry}
        setIsOpen={setIsOpen}
      />

      <MainHeader title={"Add New Transaction"} headerType={"h3"}/>
      <NewEntryForm
        description={description}
        value={value}
        isExpense={isExpense}
        setValue={setValue}
        setDescription={setDescription}
        setIsExpense={setIsExpense}
        addEntry={addEntry}
      />
      <ModalEdit
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        description={description}
        value={value}
        isExpense={isExpense}
        setValue={setValue}
        setDescription={setDescription}
        setIsExpense={setIsExpense}
        addEntry={addEntry}/>
    </Container>
  );
}

export default App;

