// reducer function
// reducers should not have side effects
// pure function: call with same action and same entries always yields same result
export default (state = initialEntries, action) => {
  let newEntries;
  switch (action.type) {
    case 'ADD_ENTRY':
      newEntries = state.concat({...action.payload});
      return newEntries;
    case 'REMOVE_ENTRY':
      newEntries = state.filter(entry => entry.id !== action.payload.id);
      return newEntries;
    case 'UPDATE_ENTRY':
      newEntries = [...state];
      const index = newEntries.findIndex(entry => entry.id === action.payload.id);
      newEntries[index] = {...action.payload.entry};
      return newEntries;
    default:
      return state;
  }
}

var initialEntries = [
  {
    id: 1,
    description: "inovex Gehalt",
    value: 1342.78,
    isExpense: false
  },
  {
    id: 2,
    description: "Auto tanken",
    value: 112.45,
    isExpense: true
  },
  {
    id: 3,
    description: "Miete",
    value: 450,
    isExpense: true
  },
  {
    id: 4,
    description: "Pugilist",
    value: 20,
    isExpense: true
  }
]