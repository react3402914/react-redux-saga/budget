import React from 'react';
import {Header} from "semantic-ui-react";

function MainHeader({title, headerType="h1"}) {
  return(
    <Header as={headerType}>
      {title}
    </Header>
  )
}
export default MainHeader;