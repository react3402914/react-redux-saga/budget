import React from 'react';
import EntryLine from "./EntryLine";

function EntryLines({entries, setIsOpen, editEntry}) {
  return(
    <>
      {entries.map((entry) => (
        <EntryLine
          key={entry.id}
          {...entry}
          editEntry={editEntry}
          setIsOpen={setIsOpen}
        />
      ))}
    </>
  )
}

export default EntryLines;