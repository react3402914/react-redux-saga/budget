import React from 'react';
import {Checkbox, Form, Segment} from "semantic-ui-react";

function EntryForm({description, value, isExpense, setDescription, setValue, setIsExpense}) {
  return (
    <>
      <Form.Group>
        <Form.Input
          placeholder={"New Shiny Thing"}
          icon={"tags"}
          width={12}
          label={"Description"}
          value={description}
          onChange={(event) => setDescription(event.target.value)}
        />
        <Form.Input
          width={4}
          label={"value"}
          placeholder={"100.00"}
          icon={"dollar"}
          type={"number"}
          iconPosition={"left"}
          value={value}
          onChange={(event) => setValue(event.target.value)}
        />
      </Form.Group>
      <Segment compact>
        <Checkbox
          toggle
          label={"is Expense"}
          checked={isExpense}
          onChange={() => setIsExpense((oldState) => !oldState)}
        />
      </Segment>
    </>
  )
}

export default EntryForm;