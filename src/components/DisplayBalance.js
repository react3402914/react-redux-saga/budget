import React from 'react';
import {Statistic} from "semantic-ui-react";

function DisplayBalance({color="black", name, amount, size="tiny"}) {
  return(
    <Statistic size={size} color={color}>
      <Statistic.Label style={{textAlign:"left"}}>
        {name}
      </Statistic.Label>
      <Statistic.Value>
        {amount} $
      </Statistic.Value>
    </Statistic>
  )
}

export default DisplayBalance;