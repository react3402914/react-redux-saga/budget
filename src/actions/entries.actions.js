// action function that is used to dispatch an action
export const addEntryRedux = (payload) => {
  return {type: 'ADD_ENTRY', payload: payload};
}

export const removeEntryRedux = (id) => {
  return {type: 'REMOVE_ENTRY', payload: {id: id}}
}

export const updateEntryRedux = (id, entry) => {
  return {type: 'UPDATE_ENTRY', payload: {id, entry}}
}