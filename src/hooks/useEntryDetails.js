import {useState} from "react";
import {useDispatch} from "react-redux";
import {addEntryRedux} from "../actions/entries.actions";
import {v4 as uuid4} from "uuid";

function useEntryDetails() {
  // states
  const [description, setDescription] = useState("");
  const [value, setValue] = useState("");
  const [isExpense, setIsExpense] = useState(true);

  // react-redux hook to dispatch actions to store
  const dispatch = useDispatch();

  function addEntry() {
    dispatch(addEntryRedux({id: uuid4(), description, value, isExpense}));
    setDescription("");
    setValue("");
    setIsExpense(true);
  }
  return {
    description,
    setDescription,
    value,
    setValue,
    isExpense,
    setIsExpense,
    addEntry
  };
}

export default useEntryDetails;